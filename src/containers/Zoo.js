import { connect } from 'react-redux'
import {setCreateModalState, setDetailModalState, handleAnimalTmpChange, setAnimal, deleteAnimalData} from '../redux/action/zoo.js'
import Zoo from '../components/Zoo/Zoo.js'

const mapStateToProps = (state, ownProps) => {
  return {
    createModalState: state.zoo.createModalState,
    detailModalState: state.zoo.detailModalState,
    animalTmp: state.zoo.animalTmp,
    animals: state.zoo.animals,
  }
}

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    setCreateModalState: (state) => {
      dispatch(setCreateModalState(state))
      if(!state){
        dispatch(handleAnimalTmpChange({validationErrors:{}}))
      }
    },
    setDetailModalState: (state, animal) => {
      dispatch(setDetailModalState(state))
      animal["validationErrors"] = {}
      dispatch(handleAnimalTmpChange(animal))
    },
    handleAnimalTmpChange: (animalTmp) => {
      dispatch(handleAnimalTmpChange(animalTmp))
    },
    setAnimal: (animals) => {
      dispatch(setAnimal(animals))
    },
    deleteAnimalData: (index) => {
      dispatch(deleteAnimalData(index))
    }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Zoo)
