export const createModalState = (state = false, action) => {
  switch (action.type) {
    case 'SET_CREATE_MODAL_STATE':
    return action.state
    default:
    return state
  }
}

export const detailModalState = (state = false, action) => {
  switch (action.type) {
    case 'SET_DETAIL_MODAL_STATE':
    return action.state
    default:
    return state
  }
}

export const animalTmp = (state = {validationErrors:{}}, action) => {
  switch (action.type) {
    case 'HANDLE_ANIMAL_TMP_CHANGE':
    let animalTmp = Object.assign({}, action.animalTmp)
    return animalTmp
    default:
    return state
  }
}

export const animals = (state = [], action) => {
  switch (action.type) {
    case 'SET_ANIMAL':
    let animals =  JSON.parse(JSON.stringify(action.animals))
    return animals
    case 'DELETE_ANIMAL_DATA':
    let newAnimals = JSON.parse(JSON.stringify(state))
    newAnimals.splice(action.index, 1)
    return newAnimals
    default:
    return state
  }
}
