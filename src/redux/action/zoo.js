export const setCreateModalState = (state) => {
  return {
    type: 'SET_CREATE_MODAL_STATE',
    state
  }
}
export const setDetailModalState = (state) => {
  return {
    type: 'SET_DETAIL_MODAL_STATE',
    state
  }
}
export const handleAnimalTmpChange = (animalTmp) => {
  return {
    type: 'HANDLE_ANIMAL_TMP_CHANGE',
    animalTmp
  }
}
export const setAnimal = (animals) => {
  return {
    type: 'SET_ANIMAL',
    animals
  }
}
export const deleteAnimalData = (index) => {
  return {
    type: 'DELETE_ANIMAL_DATA',
    index
  }
}
