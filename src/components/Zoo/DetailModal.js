import React, { PropTypes } from 'react'
import {Modal, FormGroup, FormControl, Button, Table} from 'react-bootstrap'

export default class DetailModal extends React.Component {
  constructor(props) {
    super(props)
  }

  render(){
    return (
      <Modal show={this.props.detailModalState}
        onHide={this.props.setDetailModalState.bind(this, false, {})}>
        <Modal.Header closeButton>
          <Modal.Title>Detail</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table striped bordered condensed hover>
            <tbody>
              <tr>
                <th>Name</th>
                <td>{this.props.animalTmp.name}</td>
              </tr>
              <tr>
                <th>Description</th>
                <td>{this.props.animalTmp.description} </td>
              </tr>
              <tr>
                <th>Type</th>
                <td>{this.props.animalTmp.type}</td>
              </tr>
            </tbody>
          </Table>
        </Modal.Body>
      </Modal>
    )
  }
}

// <Button bsStyle="primary" onClick={this.props.setDetailModalState.bind(this, false)}>Close</Button>
