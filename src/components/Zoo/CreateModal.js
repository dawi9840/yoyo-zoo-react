import React, { PropTypes } from 'react'
import {Modal, FormGroup, FormControl, Button, Table, Radio} from 'react-bootstrap'


export default class CreateModal extends React.Component {
  constructor(props) {
    super(props)
  }

  handleAnimalTmpChange(key, e) {
    let animalTmp = this.props.animalTmp
    animalTmp[key] = e.target.value
    this.props.handleAnimalTmpChange(animalTmp)
  }

  setAnimal() {
    let animals = this.props.animals
    animals.push(this.props.animalTmp)
    this.props.setAnimal(animals)
    this.props.setCreateModalState(false)
  }
  editAnimal(){
    let animal = this.props.animals
    animal[this.props.animalTmp.index] = this.props.animalTmp
    this.props.setAnimal(animal)
    this.props.setCreateModalState(false)
  }
  validationState() {
    let animalTmp = this.props.animalTmp
    animalTmp["validationErrors"] = {}

    if (this.props.animalTmp.name === undefined || this.props.animalTmp.name === "") {
      animalTmp["validationErrors"]["name"] = 'error'
    }
    if (this.props.animalTmp.description === undefined || this.props.animalTmp.description === "") {
      animalTmp["validationErrors"]["description"] = 'error'
    }
    if (this.props.animalTmp.type === undefined || this.props.animalTmp.type === "") {
      animalTmp["validationErrors"]["type"] = 'error'
    }

    this.props.handleAnimalTmpChange(animalTmp)

    if (!Object.keys(animalTmp["validationErrors"]).length) {
      if(this.props.animalTmp.index!=null){
        this.editAnimal()
      }
      else{
        this.setAnimal()
      }
    }
  }

  render() {
    let validationType
    if(this.props.animalTmp.validationErrors.type === 'error' ){
      validationType = [<p style={{color:"#ff0000"}}>Please chose a type</p>]
    }
console.log('animalTmp.type = ', this.props.animalTmp.type);
    return (
      <Modal show={this.props.createModalState}
        onHide={this.props.setCreateModalState.bind(this, false)}>
        <Modal.Header closeButton>
          <Modal.Title>Animal Data</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Name</p>
          <FormGroup validationState={this.props.animalTmp.validationErrors.name}>
            <FormControl type="text"
              placeholder="Name"
              onChange={this.handleAnimalTmpChange.bind(this, 'name')}
              value={this.props.animalTmp.name}/>
          </FormGroup>
          <p>Description</p>
          <FormGroup validationState={this.props.animalTmp.validationErrors.description}>
            <FormControl type="text"
              placeholder="Description"
              onChange={this.handleAnimalTmpChange.bind(this, 'description')}
              value={this.props.animalTmp.description}/>
          </FormGroup>
          <p>Type</p>
          {validationType}
          <FormGroup onChange={this.handleAnimalTmpChange.bind(this, 'type')}>
            <Radio name="radioGroup" inline value="Old" readOnly checked={this.props.animalTmp.type === 'Old' ? true : false} >
              Old
            </Radio>
            &nbsp;
            <Radio name="radioGroup" inline value="Strong" readOnly checked={this.props.animalTmp.type === 'Strong' ? true : false}>
              Strong
            </Radio>
            &nbsp;
            <Radio name="radioGroup" inline value="Young" readOnly checked={this.props.animalTmp.type === 'Young' ? true : false}>
              Young
            </Radio>
          </FormGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={this.props.setCreateModalState.bind(this, false)}>Close</Button>
          <Button bsStyle="primary" onClick={this.validationState.bind(this)}>Save</Button>
        </Modal.Footer>
      </Modal>
    )
  }
}
