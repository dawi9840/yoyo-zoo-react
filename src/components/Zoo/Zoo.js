import React, { PropTypes } from 'react'
import {Button, Col, Image, Table, ButtonToolbar, Modal} from 'react-bootstrap'
import Icon from 'react-fa'
import bootbox from 'bootbox'

import CreateModal from './CreateModal.js'
import DetailModal from './DetailModal.js'


export default class Zoo extends React.Component {
  constructor(props) {
    super(props)
    let animals = [{name: "Panda", description: "動物描述1111", type: "Young"},
    {name: "Alpaca", description: "動物描述2222", type: "Old"},
    {name: "Giraffe", description: "動物描述3333", type: "Strong"}]
    props.setAnimal(animals)
  }

  onClickEdit(animal, index){
    animal["validationErrors"] = {}
    animal["index"] = index
    this.props.handleAnimalTmpChange(animal)
    this.props.setCreateModalState(true)
  }

  onClickDelete(animal, index){
    bootbox.dialog({
      title: "Delete Data",
      message: '<div style="width:100%; white-space:nowrap; overflow:hidden; text-overflow:ellipsis">Do you want to delete the [ ' + animal.name + ' ] ?</div>',
      buttons: {
        cancel: {
          label: "Cancel",
          className: "btn-danger"
        },
        success: {
          label: "Delete",
          className: "btn-info",
          callback: (confirm) => {
            if (confirm) {
              this.props.deleteAnimalData(index)
            }
          }
        }
      }
    })
  }
  render() {
    let animals = this.props.animals.map((animal, index) => {
      return (
        <tr key={index} >
          <td><a herf="#" onClick={this.props.setDetailModalState.bind(this, true, animal)} style={{cursor:"pointer"}}>{animal.name}</a></td>
          <td>{animal.description}</td>
          <td>{animal.type}</td>
          <td>
            <Button bsStyle="info" bsSize="xsmall" onClick={this.onClickEdit.bind(this, animal, index)}>
              <Icon name="pencil"/>
            </Button>
            &nbsp;
            <Button bsStyle="danger" bsSize="xsmall" onClick={this.onClickDelete.bind(this, animal, index)}>
              <Icon name="trash-o"/>
            </Button>
          </td>
        </tr>
      )
    })
    return (
      <div className="container">
        <h1>Zoos For Table!</h1>
        <Button bsStyle="info" className="pull-right" onClick={this.props.setCreateModalState.bind(this, true)}>Add Data</Button>
        <br/><br/>
        <Table responsive>
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Type</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {animals}
          </tbody>
        </Table>
        <CreateModal {...this.props} />
        <DetailModal {...this.props} />
      </div>
    )
  }
}
